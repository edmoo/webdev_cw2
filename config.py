import os

WTF_CSRF_ENABLED = True

# CSRF Key
SECRET_KEY = 'K3#4!mdfi9'

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True
