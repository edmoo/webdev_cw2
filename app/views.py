from flask import render_template, flash, request, make_response
from app import app, db, models
from .forms import AssessmentForm
from .forms import IdForm
from .forms import UserForm
from .forms import PasswordForm

#Home page which displays all assessments. home() returns all assessments in the database
@app.route('/', methods=['GET', 'POST'])
def home():
    darkMd= request.cookies.get('variable_name')
    p=models.Assessment.query.all()
    home={'description':p}
    x=models.Logged.query.filter_by(completed=True).first()
    if x == None:
        x=models.Logged(username='',completed=False)
    return render_template('home.html',
						   title='Home',home=home, x=x, darkMd=darkMd)

#Create new assessments to put into the database
@app.route('/create', methods=['GET', 'POST'])
def create():
    darkMd= request.cookies.get('variable_name')
    form = AssessmentForm()
    x=models.Logged.query.filter_by(completed=True).first()
    if x == None:
        x=models.Logged(username='',completed=False)
	#If all forms have valid data input
    if form.validate_on_submit():
        #Create a new assessment
        p=models.Assessment(title=form.title.data,moduleCode=form.modCode.data,
		start_date=form.deadline.data,descriptionCol=form.desc.data)
        #insert new assessment into the sessions database
        db.session.add(p)
        #commit the database to save the new model
        db.session.commit()
        flash('%s has been saved.'%(form.title.data))
    return render_template('create.html',
                           title='Create Assessment',
                           form=form, x=x, darkMd=darkMd)

#Returns all the movie models, same as the home page.
#You can mark them as seen, removing them from this page
@app.route('/uncomplete', methods=['GET', 'POST'])
def uncomplete():
    darkMd= request.cookies.get('variable_name')
	#IdForm which holds the Id of each movie
    form = IdForm()
    k=models.Logged.query.filter_by(completed=True).first()
    if k == None:
        k=models.Logged(username='',completed=False)
	#Returns all models to be potentially listed
    x=models.Assessment.query.all()
    uncomplete={'description':x}
	#If user clicks Mark as Complete, the model with the same id is marked.
    if form.validate_on_submit():
        pId = int(form.idHolder.data)
        pFind=models.Assessment.query.get(pId)
        pFind.completed=True
        db.session.commit()
    return render_template('uncomplete.html',
                           title='Uncomplete Assessments',
                           form=form,
						   uncomplete=uncomplete, x=k, darkMd=darkMd)

@app.route('/user/add', methods=['GET', 'POST'])
def add_user():
    darkMd= request.cookies.get('variable_name')
    form = UserForm()
    x=models.Logged.query.filter_by(completed=True).first()
    if x == None:
        x=models.Logged(username='',completed=False)
    if form.validate_on_submit():
        p=models.User(username=form.username.data, password=form.password.data)
        userLog = models.User.query.filter_by(username=p.username).first()
        if userLog != None:
            if userLog.password==p.password:
                return render_template('usernameTaken.html',
                               title='Username Taken', x=x, darkMd=darkMd)
        db.session.add(p)
        db.session.commit()
        return render_template('accountCreated.html',
                               title='Account Created', x=x, darkMd=darkMd)
    return render_template('add_user.html',
                           title='Add User',
                           form=form, x=x, darkMd=darkMd)

@app.route('/user/login', methods=['GET', 'POST'])
def login_user():
    darkMd= request.cookies.get('variable_name')
    form = UserForm()
    x=models.Logged.query.filter_by(completed=True).first()
    if x == None:
        x=models.Logged(username='',completed=False)
    if form.validate_on_submit():
        p=models.User(username=form.username.data, password=form.password.data)
        userLog = models.User.query.filter_by(username=p.username).first()
        if userLog != None:
            if userLog.password==p.password:
                x = models.Logged(username=p.username,completed=True)
                db.session.add(x)
                db.session.commit()
                return render_template('logged.html',
                                       title='Logged in', x=x, darkMd=darkMd)
        return render_template('wrongDetails.html',
		                        title='Login Details Wrong', x=x, darkMd=darkMd)
    return render_template('Login_user.html',
                           title='Login User',
                           form=form, x=x, darkMd=darkMd)

@app.route('/user/logout', methods=['GET', 'POST'])
def logout_user():
    darkMd= request.cookies.get('variable_name')
    models.Logged.query.delete()
    db.session.commit()
    x=models.Logged.query.filter_by(completed=True).first()
    if x == None:
        x=models.Logged(username='',completed=False)
    return render_template('logout_user.html',
	                        title='Logout User', x=x, darkMd=darkMd)

@app.route('/user/settings', methods=['GET', 'POST'])
def settings():
    darkMd= request.cookies.get('variable_name')
    x=models.Logged.query.filter_by(completed=True).first()
    if x == None:
        x=models.Logged(username='',completed=False)
    form = PasswordForm()
    p=models.User.query.filter_by(username=x.username).first()
    if form.validate_on_submit():
        newPass = form.password.data
        p.password = newPass
        db.session.commit()
    return render_template('settings.html',
	                        title='User settings', x=x, form=form,
                              userPass=p.password, darkMd=darkMd)

@app.route('/user/logout', methods=['GET', 'POST'])
def loggedIn():
    darkMd= request.cookies.get('variable_name')
    x=models.Logged.query.filter_by(completed=True).first()
    if x == None:
        x=models.Logged(username='',completed=False)
    return render_template('logged.html',
	                        title='Logged In', x=x, darkMd=darkMd)


@app.route('/setcookie', methods = ['POST', 'GET'])
def setcookie():
    if request.method == 'POST':
        if request.cookies.get('variable_name') == 'dark':
            value = 'light'
        else:
            value = 'dark'
    resp = make_response(render_template('cookie.html'))
    resp.set_cookie('variable_name', value)
    return resp

@app.route('/getcookie')
def getcookie():
   name = request.cookies.get('variable_name')
   return name
