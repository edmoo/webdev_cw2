from app import db

# Will be populated with data from the AssessmentForm except for completed, which is default set to false
class Assessment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(500), index=True)
    moduleCode = db.Column(db.String(500), index=True);
    start_date = db.Column(db.DateTime)
    descriptionCol = db.Column(db.String(500), index=True)
    completed = db.Column(db.Boolean, default=False)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(500), index=True, unique=True);
    password = db.Column(db.String(500),index=True)

class Logged(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(500), index=True, unique=True);
    completed = db.Column(db.Boolean, default=False)
