from flask_wtf import Form
from wtforms import TextField
from wtforms import DateField
from wtforms import TextAreaField
from wtforms import IntegerField
from wtforms.validators import DataRequired, Length

# Contains all forms required for the creation of an assessment according to the assessment model in models.py
class AssessmentForm(Form):
    title = TextField('title', validators=[DataRequired(),Length(max=75)])
    modCode = TextField('modCode', validators=[DataRequired(),Length(max=20)])
    deadline = DateField('deadline', format='%d-%m-%Y',validators=[DataRequired()])
    desc = TextAreaField('desc',validators=[DataRequired(),Length(max=75)])

# Holds ID of assessment model in uncomplete.html
class IdForm(Form):
    idHolder = IntegerField('idHolder', validators=[DataRequired()])

class UserForm(Form):
    username = TextField('username', validators=[DataRequired(),Length(max=20)])
    password = TextField('password',validators=[DataRequired(),Length(max=75)])

class PasswordForm(Form):
    password = TextField('password',validators=[DataRequired(),Length(max=75)])

class SessionForm(Form):
    value = TextField('value', validators=[DataRequired()])
